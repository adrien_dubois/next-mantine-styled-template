'use client';
import { ThemeProvider } from "styled-components";
import GlobalStyles from "./GlobalStyles";
import { useEffect, useState } from "react";
import { useDarkMode } from "@/hooks/useDarkMode";

const GlobalStylesProvider = ({ 
  children 
}: { 
    children: JSX.Element | JSX.Element[]
  }) => {

    const [theme] = useDarkMode();

    const [mounted, setMounted] = useState(false);

    useEffect(() => {
      setMounted(true);
    }, []);
  
    if (!mounted) return <></>;

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {children}
    </ThemeProvider>
  );
};

export default GlobalStylesProvider;
