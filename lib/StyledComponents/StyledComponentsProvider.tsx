'use client'

import React, { useState } from "react"
import { ServerStyleSheet, StyleSheetManager } from "styled-components";
import { useServerInsertedHTML } from 'next/navigation';

export default function StyledComponentsProvider({
    children
}: {
    children: React.ReactNode;
}) {
    const [styledComponentsStylesheet] = useState(() => new ServerStyleSheet());

    useServerInsertedHTML (() => {
        const styles = styledComponentsStylesheet.getStyleElement();
        styledComponentsStylesheet.instance.clearTag();
        
        return <> { styles } </>
    });

    if(typeof window !== 'undefined')
    return(
        <>
            { children }
        </>
    );

    return(
        <StyleSheetManager sheet={styledComponentsStylesheet.instance}>
            { children }
        </StyleSheetManager>
    )
}
