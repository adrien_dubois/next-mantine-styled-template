"use client";

import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
    :root{
        /*------ FONTS ------*/
        --roboto: 'Roboto', sans-serif;
        --rubik : 'Rubik',  sans-serif;
        --poppins : 'Poppins',  sans-serif;

        /*----- COLORS -----*/
        --bg           : #FFF;
        --textColor    : #000;
        --softBg       : #F0F0F0;
        --softTextColor: #626262;
        --wh-10        : #F4F4F4;
        --wh-50        : #FBFBFB;
        --wh-100       : #C9C9C9;
        --wh-300       : #939393;
        --wh-400       : #818181;
        --wh-500       : #595959;
        --wh-900       : #0F0F0F;
        --error-color  : #F43B47;
        --light-bg     : #F5F6F8;
    }

    :root,
    html,
    body{
        height: 100%;
        width : 100%;
    }

    * {
        margin         : 0;
        padding        : 0;
        list-style-type: none;
        list-style     : none;
        outline        : none;
        border         : none;
        text-decoration: none;
        box-sizing     : border-box;
    }

    body{
        font-family            : var(--roboto);
        max-width              : 100vw;
        overflow-x             : hidden;
        font-smooth            : always;
        -webkit-font-smoothing : antialiased;
        -moz-osx-font-smoothing: grayscale;
        display                : block;
        background-color: ${props => props.theme.bgLighter};
        color: ${props => props.theme.textColor};
    }
    
    a{
        color: inherit;
        text-decoration: none;
    }
`;

export default GlobalStyles;