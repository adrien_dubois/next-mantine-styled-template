/** @type {import('next').NextConfig} */
const nextConfig = {
    compiler: {
      styledComponents: true,
    },
    experimental: {
      serverActions: true
    },
    reactStrictMode: false
  }

module.exports = nextConfig
