import { Poppins, Roboto, Rubik } from 'next/font/google'

export const roboto = Roboto({
    weight: [ '100', '300', '400', '500', '700', '900'],
    style: ['normal', 'italic'],
    variable: '--roboto-font',
    subsets: ['latin']
  })

export const rubik = Rubik({
    weight: ['300', '400', '500', '600', '700', '800'],
    style: ['normal', 'italic'],
    variable: '--rubik-font',
    subsets: ['latin']
})

export const poppins = Poppins({
  subsets: ["latin"],
  weight: ["400", "600"],
  variable: '--poppins-font'
});