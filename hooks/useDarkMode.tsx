'use client'
import { darkTheme } from "@/lib/styles/themes/darkTheme";
import { lightTheme } from "@/lib/styles/themes/lightTheme";
import { useEffect, useState } from "react"
import { useLocalStorage } from "usehooks-ts";

/**
 * Personnal hook to set the dark or light mode with styled component
 * @returns 
 */
export const useDarkMode = (): [typeof lightTheme, (() => void), ConstrainBoolean] => {
    
    const [theme, setTheme] = useLocalStorage('theme', lightTheme)
    
    const [mountedComponent, setMountedComponent] = useState<boolean>(false);

    // If Dark mode is enabled in Chrome, get it
    const prefersDark =
        typeof window !== 'undefined' && window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;
    
    const isDark = theme.name === 'dark';

    // Change theme
    const themeToggler = () => {
        isDark ? setTheme(lightTheme) : setTheme(darkTheme);
    };

    useEffect(() => {
        // If there is already a defined theme biy the app get it
        const localTheme = theme

        // If there is no theme defined by the app, we will check the user preferences and set the theme in function of it
        if(!localTheme){
            if(prefersDark){
                setTheme(darkTheme);
            }
            else if(!prefersDark){
                setTheme(lightTheme);
            }
        }

        // Finally set the theme et declare that component is mounted
        localTheme && setTheme(localTheme);
        setMountedComponent(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return [theme, themeToggler, mountedComponent];
 }