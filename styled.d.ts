import {} from 'styled-components';
import { lightTheme } from './lib/styles/themes/lightTheme';
declare module 'styled-components' {
    type Theme = typeof lightTheme;
    export interface DefaultTheme extends Theme {}
}